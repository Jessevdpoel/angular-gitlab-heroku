import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { Developer } from '../developer/developer'
import { DeveloperDetailComponent } from './developer-detail.component'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { Component } from '@angular/core'
import { NgbDatepicker, NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'

@Component({
  selector: 'app-asdasd',
  template: '<app-developer-detail [developer]="input"></app-developer-detail>'
})
class TestHostComponent {
  input = {
    name: 'nintendo',
    founder: 'mario',
    founded: new Date(),
    employees: 123123123
  }
}
describe('DeveloperDetailComponent', () => {
  let component: TestHostComponent
  let fixture: ComponentFixture<TestHostComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeveloperDetailComponent, TestHostComponent],
      imports: [FormsModule, HttpClientModule, NgbModule, RouterTestingModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

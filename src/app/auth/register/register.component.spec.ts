import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RegisterComponent } from './register.component'
import { ReactiveFormsModule } from '@angular/forms'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientModule } from '@angular/common/http'
import { LoginComponent } from '../login/login.component'

describe('RegisterComponent', () => {
  let component: RegisterComponent
  let fixture: ComponentFixture<RegisterComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule, HttpClientTestingModule],
      providers: [AlertService, LoginComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  function updateForm(userEmail, userPassword, userName) {
    component.registerForm.controls['name'].setValue(userName)
    component.registerForm.controls['email'].setValue(userEmail)
    component.registerForm.controls['password'].setValue(userPassword)
  }

  it('form value should update from when u change the input', () => {
    const validUser = {
      name: 'test',
      email: 'test@avans.nl',
      password: '12312'
    }
    updateForm(validUser.email, validUser.password, validUser.name)
    expect(component.registerForm.value).toEqual(validUser)
  })

  it('Form invalid should be true when form is invalid', () => {
    const blankUser = {
      name: '',
      email: '',
      password: ''
    }

    updateForm(blankUser.email, blankUser.password, blankUser.name)
    expect(component.registerForm.invalid).toBeTruthy()
  })
  // tslint:enable:object-literal-key-quotes
})

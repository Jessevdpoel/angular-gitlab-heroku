const express = require('express')
const router = express.Router()
const User = require('../models/user.model')
const logger = require('../config/applicationConfig').logger
const jwt = require('jsonwebtoken')

router.post('/', async function(req, res) {
  try {
    logger.info('POST thread summoned')
    const user = req.body

    // checking the user is not a duplicate
    const foundUser = await User.find({ email: user.email }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (user.password === foundUser[0].password) {
      // saving the user
      const user = new User(foundUser[0])
      console.log(req.body)
      // tracing received object
      const token = jwt.sign({ id: user._id }, req.app.get('secretkey'))
      logger.trace(user)
      await user
        .save()
        .then(res.status(200).json({ message: 'auth succesful', token: token }))
        .catch(function(err) {
          res.status(400).end('Something went wrong')
          logger.error(err)
        })
    } else {
      res.status(406).end('auth unsuccesfull')
    }
    // Error Handling
  } catch (err) {
    res.status(400).end()
    logger.error(err)
  }

  logger.info('POST succesful')
})

module.exports = router

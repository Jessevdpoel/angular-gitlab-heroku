import { Developer } from '../../developers/developer/developer'

export class Game {
  name: string
  releaseDate: Date
  developer: Developer
  rating: number
  description: string
}

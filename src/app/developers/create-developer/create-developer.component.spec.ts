import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CreateDeveloperComponent } from './create-developer.component'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { RouterTestingModule } from '@angular/router/testing'

describe('CreateDeveloperComponent', () => {
  let component: CreateDeveloperComponent
  let fixture: ComponentFixture<CreateDeveloperComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateDeveloperComponent],
      imports: [FormsModule, HttpClientModule, NgbModule, RouterTestingModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDeveloperComponent)
    component = fixture.componentInstance

    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

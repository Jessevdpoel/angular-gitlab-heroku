import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CharacterDetailComponent } from './character-detail.component'
import { Component } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'

@Component({
  selector: 'app-asdasd',
  template: '<app-character-detail [character]="input"></app-character-detail>'
})
class TestHostComponent {
  input = {
    name: 'nintendo',
    series: 'mario'
  }
}

describe('TestHostComponent', () => {
  let component: TestHostComponent
  let fixture: ComponentFixture<TestHostComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharacterDetailComponent, TestHostComponent],
      imports: [FormsModule, HttpClientModule, RouterTestingModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

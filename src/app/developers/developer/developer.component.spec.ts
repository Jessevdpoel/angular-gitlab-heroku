import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { HttpClientModule } from '@angular/common/http'
import { DeveloperComponent } from './developer.component'
import { FormsModule } from '@angular/forms'
import { Component, Input } from '@angular/core'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-developer-detail',
  template: ''
})
class DeveloperDetailSubComponent {
  @Input() developer
}

describe('DeveloperComponent', () => {
  let component: DeveloperComponent
  let fixture: ComponentFixture<DeveloperComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeveloperComponent, DeveloperDetailSubComponent],
      imports: [HttpClientModule, FormsModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DeveloperComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

import { TestBed } from '@angular/core/testing'

import { CharacterService } from './character.service'
import { HttpClientModule } from '@angular/common/http'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('CharacterService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule]
    })
  )

  it('should be created', () => {
    const service: CharacterService = TestBed.get(CharacterService)
    expect(service).toBeTruthy()
  })
})

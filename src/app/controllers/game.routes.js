const express = require('express')
const router = express.Router()
const Game = require('../models/game.model')
const logger = require('../config/applicationConfig').logger
const jwt = require('jsonwebtoken')

router.post('/', async function(req, res) {
  try {
    logger.info('POST thread summoned')

    const authorization = req.headers.authorization.split(' ')[1]
    let userId = jwt.verify(authorization, req.app.get('secretkey'))
    req.body.user = userId.id

    // checking the game is not a duplicate
    const foundGame = await Game.find({ name: req.body.name }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (foundGame[0] === undefined) {
      // saving the game
      const game = new Game(req.body)
      console.log(req.body)
      // tracing received object
      logger.trace(game)
      await game
        .save()
        .then(res.status(200).send({ name: game.name }))
        .catch(function(err) {
          res.status(400).end('Something went wrong')
          logger.error(err)
        })
    } else {
      res.status(406).send(foundGame)
    }
    // Error Handling
  } catch (err) {
    res.status(400).end()
    logger.error(err)
  }

  logger.info('POST succesful')
})

// Get All
router.get('/', async function(req, res) {
  logger.info('GET all games summoned')

  // Defining variables

  // Finding all games
  const games = await Game.find()
    .populate('games')
    .catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

  // Response status
  res.status(200).send(games)
})

// Delete thread on id
router.delete('/:name', async function(req, res) {
  logger.info('DELETE Game by name summoned')

  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id
  // Defining variables
  const gameName = req.params.name
  // Trying to find the game by Name
  try {
    // Searching by Name
    let foundGame = await Game.find({ name: gameName }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (req.body.user !== foundGame[0].user.toString()) {
      res.status(403).end({
        message: 'Not allowed to modify other users entity!',
        code: 403
      })
    }

    // If there is no game a bad request is send
    if (gameName === foundGame[0].name) {
      try {
        await Game.deleteOne({ name: gameName }).catch(function(error) {
          logger.error(error)
          res.status(400).send(error)
        })
        // Response status
        res.status(200).send(foundGame)
        // Error handling
      } catch (err) {
        res.status(400).end(err)
      }
      // Error handling for if there is no game found
    } else {
      res.status(400).end('No Game found, are you sure you entered the right name?')
    }
    // Error handling for if the name does not exist
  } catch (err) {
    res.status(204).end('Naam bestaat niet in de database ')
  }
})

router.put('/:name', async function(req, res) {
  logger.info('PUT by Name summoned')

  const releaseDate = req.body.releaseDate
  const description = req.body.description
  const rating = req.body.rating
  const developer = req.body.developer
  const gameName = req.params.name

  // Defining variables
  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id
  // Trying to find the developer by Name
  try {
    // Searching by Name
    let foundGame = await Game.find({ name: gameName }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })
    console.log(foundGame)
    console.log(req.body.user)
    if (req.body.user !== foundGame[0].user.toString()) {
      res.status(403).end({
        message: 'Not allowed to modify other users entity!',
        code: 403
      })
    }

    // If there is no developer a bad request is send
    if (gameName === foundGame[0].name) {
      try {
        await Game.updateOne(
          { name: gameName },
          {
            $set: { releaseDate: releaseDate, description: description, rating: rating, developer: developer }
          }
        ).catch(function(error) {
          res.status(400).send(error)
          logger.error(error)
        })

        // Response status
        res.status(200).send(foundGame)
        // Error handling
      } catch (err) {
        res.status(400).end(err)
      }
      // Error handling
    } else {
      logger.info('Something went wrong, please call: 0630866874 or 0642979245')
      res.status(400).end('No Character found')
    }
    // Error handling
  } catch (err) {
    res.status(204).end('Naam bestaat niet hoor')
  }
})

module.exports = router

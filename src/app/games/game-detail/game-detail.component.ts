import { Component, OnInit, Input } from '@angular/core'
import { Game } from '../../games/game/game'
import { GameService } from '../../game.service'
import { DeveloperService } from '../../developer.service'
import { Developer } from '../../developers/developer/developer'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {
  date
  developers$: Developer[]

  constructor(
    private gameService: GameService,
    private developerService: DeveloperService,
    private alertService: AlertService,
    private router: Router
  ) {}
  @Input() game: Game
  ngOnInit() {
    this.developerService.getDevelopers().subscribe(data => (this.developers$ = data))
  }

  save(): void {
    this.game.releaseDate = new Date(this.date.year, this.date.month, this.date.day)

    this.gameService.putGamesByName(this.game).subscribe(data => {
      if (data === null) {
        this.alertService.error('Not allowed to update other users entities')
      } else {
        this.router.navigate(['/about'])
        this.alertService.success('Entity updated succesfully!')
      }
    })
  }

  delete(): void {
    this.gameService.deleteGames(this.game).subscribe(data => {
      if (data === null) {
        this.alertService.error('Not allowed to update other users entities')
      } else {
        this.router.navigate(['/about'])
        this.alertService.success('Entity updated succesfully!')
      }
    })
  }
}

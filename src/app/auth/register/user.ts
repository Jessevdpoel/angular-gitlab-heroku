export class User {
  name: string
  email: string
  password: string
  token: string

  constructor(values: any = {}) {
    // Assign all values to this objects properties
    Object.assign(this, values)
    // In our case, the server returns a different set of name properties.
    // Here we map them to our properties.
    if (values.email) {
      this.email = values.email
    }
  }
}

import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { GameComponent } from './games/game/game.component'
import { DeveloperComponent } from './developers/developer/developer.component'
import { CharacterComponent } from './characters/character/character.component'
import { CreateDeveloperComponent } from './developers/create-developer/create-developer.component'
import { CreateCharacterComponent } from './characters/create-character/create-character.component'
import { CreateGameComponent } from './games/create-game/create-game.component'
import { AuthGuard } from './auth/auth.guard'
import { LoginComponent } from './auth/login/login.component'
import { RegisterComponent } from './auth/register/register.component'

const routes: Routes = [
  { path: '', redirectTo: 'about', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'about', component: UsecasesComponent },
  { path: 'games', component: GameComponent, canActivate: [AuthGuard] },
  { path: 'games/new', component: CreateGameComponent, canActivate: [AuthGuard] },
  { path: 'developers', component: DeveloperComponent, canActivate: [AuthGuard] },
  { path: 'developers/new', component: CreateDeveloperComponent, canActivate: [AuthGuard] },
  { path: 'characters', component: CharacterComponent, canActivate: [AuthGuard] },
  { path: 'characters/new', component: CreateCharacterComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { JwtHelperService } from '@auth0/angular-jwt'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { CharacterComponent } from './characters/character/character.component'
import { DeveloperComponent } from './developers/developer/developer.component'
import { GameComponent } from './games/game/game.component'
import { DeveloperDetailComponent } from './developers/developer-detail/developer-detail.component'
import { CreateDeveloperComponent } from './developers/create-developer/create-developer.component'
import { CharacterDetailComponent } from './characters/character-detail/character-detail.component'
import { CreateCharacterComponent } from './characters/create-character/create-character.component'
import { GameDetailComponent } from './games/game-detail/game-detail.component'
import { CreateGameComponent } from './games/create-game/create-game.component'

import { AlertModule } from './modules/alert/alert.module'

import { AlertService } from './modules/alert/alert.service'
import { AlertComponent } from './modules/alert/alert.component'
import { JwtInterceptor } from './auth/jwt.interceptor'
import { LoginComponent } from './auth/login/login.component'
import { RegisterComponent } from './auth/register/register.component'
import { AuthGuard } from './auth/auth.guard'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    CharacterComponent,
    DeveloperComponent,
    DeveloperDetailComponent,
    GameComponent,
    CreateDeveloperComponent,
    CharacterDetailComponent,
    CreateCharacterComponent,
    GameDetailComponent,
    CreateGameComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AlertModule,
    RouterModule,
    ReactiveFormsModule,
    NgbModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    AlertService,
    AuthGuard,
    JwtHelperService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    LoginComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

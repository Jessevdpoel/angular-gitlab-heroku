const mongoose = require('mongoose')
const Schema = mongoose.Schema

const DeveloperSchema = new Schema({
  name: {
    type: String,
    required: [true, 'creating a Developer requires a name'],
    unique: true
  },
  founded: {
    type: Date,
    required: [true, 'Creating a Developer requires a founded']
  },
  founder: {
    type: String,
    required: [true, 'Creating a Developer requires a founder']
  },
  employees: {
    type: Number,
    required: [true, 'Creating a Developer requires employees']
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Creating developer model

const Developer = mongoose.model('developer', DeveloperSchema)

// Exporting the Developer
module.exports = Developer

const express = require('express')
const router = express.Router()
const Character = require('../models/character.model')
const logger = require('../config/applicationConfig').logger
const jwt = require('jsonwebtoken')

router.post('/', async function(req, res) {
  try {
    logger.info('POST thread summoned')

    const authorization = req.headers.authorization.split(' ')[1]
    let userId = jwt.verify(authorization, req.app.get('secretkey'))
    req.body.user = userId.id

    // checking the character is not a duplicate
    const foundCharacter = await Character.find({ name: req.body.name }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (foundCharacter[0] === undefined) {
      // saving the character
      const character = new Character(req.body)
      console.log(req.body)
      // tracing received object
      logger.trace(character)
      await character
        .save()
        .then(res.status(200).send({ name: character.name }))
        .catch(function(err) {
          res.status(400).end('Something went wrong')
          logger.error(err)
        })
    } else {
      res.status(406).send(foundCharacter)
    }
    // Error Handling
  } catch (err) {
    res.status(400).end()
    logger.error(err)
  }

  logger.info('POST succesful')
})

// Get All
router.get('/', async function(req, res) {
  logger.info('GET all characters summoned')

  // Defining variables

  // Finding all characters
  const characters = await Character.find()
    .populate('characters')
    .catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

  // Response status
  res.status(200).send(characters)
})

router.get('/:name', async function(req, res) {
  logger.info('GET Character by name summoned')
  // Defining variables
  const characterName = req.params.name
  // Requesting server for specific thread
  try {
    // Finding thread by ID
    let foundCharacter = await Character.find({ name: characterName }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    // If there is no thread a bad request is send as respons
    if (characterName === foundCharacter[0].name) {
      try {
        // Response status
        res.status(200).send(foundCharacter)
      } catch (err) {
        // Response status
        res.status(404).end(err)
      }
    } else {
      logger.info('er ging iets fout bel alstublieft: 0642979245')
    }
  } catch (err) {
    // Response status

    res.status(204).end('Id bestaat niet')
  }
})

// Delete Character on id
router.delete('/:name', async function(req, res) {
  logger.info('DELETE Character by name summoned')

  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  // Defining variables
  const characterName = req.params.name
  // Trying to find the character by Name
  try {
    // Searching by Name
    let foundCharacter = await Character.find({ name: characterName }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })
    console.log(foundCharacter[0].user.toString())
    if (req.body.user !== foundCharacter[0].user.toString()) {
      res.status(403).end({
        message: 'Not allowed to modify other users entity!',
        code: 403
      })
    }

    // If there is no character a bad request is send
    if (characterName === foundCharacter[0].name) {
      try {
        await Character.deleteOne({ name: characterName }).catch(function(error) {
          res.status(400).send(error)
          logger.error(error)
        })

        // Response status
        res.status(200).send(foundCharacter)
        // Error handling
      } catch (err) {
        res.status(400).end(err)
      }
      // Error handling
    } else {
      logger.info('Something went wrong, please call: 0630866874 or 0642979245')
      res.status(400).end('No Character found')
    }
    // Error handling
  } catch (err) {
    res.status(204).end('Naam bestaat niet')
  }
})

router.put('/:name', async function(req, res) {
  logger.info('PUT by Name summoned')

  // Defining variables
  const characterName = req.params.name
  // Trying to find the character by Name
  try {
    // Searching by Name
    let foundCharacter = await Character.find({ name: characterName }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (req.body.user !== foundCharacter[0].user.toString()) {
      res.status(403).end({
        message: 'Not allowed to modify other users entity!',
        code: 403
      })
    }

    // If there is no character a bad request is send
    if (characterName === foundCharacter[0].name) {
      try {
        await Character.updateOne({ name: characterName }, { $set: { series: series } }).catch(function(
          error
        ) {
          res.status(400).send(error)
          logger.error(error)
        })

        // Response status
        res.status(200).send(foundCharacter)
        // Error handling
      } catch (err) {
        res.status(400).end(err)
      }
      // Error handling
    } else {
      logger.info('Something went wrong, please call: 0630866874 or 0642979245')
      res.status(400).end('No Character found')
    }
    // Error handling
  } catch (err) {
    res.status(204).end('Naam bestaat niet')
  }
})

module.exports = router

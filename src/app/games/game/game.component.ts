import { Component, OnInit } from '@angular/core'
import { Game } from './game'
import { GameService } from '../../game.service'

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  games$: Game[]
  selectedGame: Game

  constructor(private gameService: GameService) {
    this.games$ = []
  }

  ngOnInit() {
    this.gameService.getGames().subscribe(data => (this.games$ = data))
  }

  onSelect(game: Game): void {
    this.selectedGame = game
  }
}

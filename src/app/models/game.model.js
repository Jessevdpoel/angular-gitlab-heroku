const mongoose = require('mongoose')
const Schema = mongoose.Schema

const GameSchema = new Schema({
  name: {
    type: String,
    required: [true, 'creating a Game requires a name'],
    unique: true
  },
  releaseDate: {
    type: Date,
    required: [true, 'Creating a Game requires a ReleaseDate']
  },
  developer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'developer',
    required: [true, 'Creating a Game requires a Developer']
  },
  rating: {
    type: Number,
    required: [true, 'Creating a Game requires rating']
  },
  description: {
    type: String,
    required: [true, 'Creating a Game requires a description']
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Creating game model
const Game = mongoose.model('game', GameSchema)

// Exporting the Game
module.exports = Game

const express = require('express')
const path = require('path')
const http = require('http')
const compression = require('compression')
const mongoose = require('mongoose')
const logger = require('./app/config/applicationConfig').logger
const developerRoute = require('../src/app/controllers/developer.routes')
const characterRoute = require('../src/app/controllers/character.routes')
const loginRoute = require('../src/app/controllers/login.routes')
const registerRoute = require('../src/app/controllers/register.routes')
const gameRoute = require('../src/app/controllers/game.routes')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()

// Compress static assets to enhance performance.
// Decrease the download size of your app through gzip compression:
app.use(compression())
app.use(cors({ origin: '*' }))
app.use(bodyParser.json())

app.set('secretkey', 'secret')

//Add connection to database

mongoose
  .connect(
    'mongodb+srv://jgpoel:jessegilliam1997@clientside-myuld.mongodb.net/Clientside?retryWrites=true&w=majority',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    }
  )
  .then(() => {
    logger.trace('MongoDB connection established')
    app.emit('databaseConnected')
  })
  .catch(err => {
    logger.error('MongoDB connection failed')
    logger.error(err)
  })

app.use('/api/developers', developerRoute)
app.use('/api/characters', characterRoute)
app.use('/api/games', gameRoute)
app.use('/api/login', loginRoute)
app.use('/api/register', registerRoute)

// appname is the name of the "defaultProject" value that was set in the angular.json file.
// When built in production mode using 'ng build --prod', a ./dist/{appname} folder is
// created, containing the generated application. The appname points to that folder.
//
// Replace the name below to match your own "defaultProject" value!
//
const appname = 'angular-gitlab-heroku'

// Point static path to dist
app.use(express.static(path.join(__dirname, '..', 'dist', appname)))

// Catch all routes and return the index file
app.use('/', function(req, res) {
  res.sendFile(path.join(__dirname, `../dist/${appname}/`, 'index.html'))
})

const port = process.env.PORT || '3000'

// Create HTTP server.
// const server = http.createServer(app)
// Listen on provided port, on all network interfaces.
app.listen(port, () => console.log(`Angular app \'${appname}\' running on port ${port}`))

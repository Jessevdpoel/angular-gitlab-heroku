import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { MessageService } from './message.service'
import { Observable, of } from 'rxjs'
import { tap, catchError } from 'rxjs/operators'
import { Character } from './characters/character/character'
import { AlertService } from './modules/alert/alert.service'

@Injectable({
  providedIn: 'root'
})
export class CharacterService {
  apiUrlCharacter = '/api/characters'
  constructor(private http: HttpClient, private messageService: MessageService) {}

  getCharacters(): Observable<Character[]> {
    return this.http.get<Character[]>(this.apiUrlCharacter).pipe(
      tap(_ => this.log('fetched Characters')),
      catchError(this.handleError<Character[]>('getCharacters', []))
    )
  }

  putCharactersByName(character: Character): Observable<any> {
    const url = this.apiUrlCharacter + '/' + character.name
    return this.http.put<any>(url, character).pipe(
      tap(_ => this.log('updated Characters')),
      catchError(this.handleError<Character>('putCharacters'))
    )
  }

  deleteCharacters(character: Character) {
    const url = this.apiUrlCharacter + '/' + character.name
    return this.http.delete<Character>(url).pipe(
      tap(_ => this.log('deleted Characters')),
      catchError(this.handleError<Character>('deleteCharacters'))
    )
  }

  postCharacters(character: Character) {
    const url = this.apiUrlCharacter
    return this.http.post<Character>(url, character).pipe(
      tap(_ => this.log('posted Characters')),
      catchError(this.handleError<Character>('postCharacters'))
    )
  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`)
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}

import { Component, Input, OnInit } from '@angular/core'
import { Observable } from 'rxjs'
import { AuthService } from 'src/app/auth/auth.service'

@Component({
  selector: 'app-navbar',
  template: `
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <div class="container">
        <a
          class="navbar-brand"
          routerLink="/"
          [routerLinkActive]="['active']"
          [routerLinkActiveOptions]="{ exact: true }"
          >{{ title }}</a
        >
        <button
          class="navbar-toggler hidden-sm-up"
          type="button"
          (click)="isNavbarCollapsed = !isNavbarCollapsed"
          data-target="#navbarsDefault"
          aria-controls="navbarsDefault"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div [ngbCollapse]="isNavbarCollapsed" class="collapse navbar-collapse" id="navbarsDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
              <div *ngIf="isLoggedIn$ | async" ngbDropdown class="d-inline-block">
                <button class="btn btn-link" id="dropdownMenu1" ngbDropdownToggle>
                  Lists
                </button>
                <div ngbDropdownMenu class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <button
                    class="dropdown-item"
                    routerLink="games"
                    [routerLinkActive]="['active']"
                    [routerLinkActiveOptions]="{ exact: true }"
                  >
                    Games
                  </button>
                  <button
                    class="dropdown-item"
                    routerLink="characters"
                    [routerLinkActive]="['active']"
                    [routerLinkActiveOptions]="{ exact: true }"
                  >
                    Game characters
                  </button>
                  <button
                    class="dropdown-item"
                    routerLink="developers"
                    [routerLinkActive]="['active']"
                    [routerLinkActiveOptions]="{ exact: true }"
                  >
                    Game developers
                  </button>
                </div>
              </div>
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                routerLink="about"
                [routerLinkActive]="['active']"
                [routerLinkActiveOptions]="{ exact: true }"
                >About</a
              >
            </li>
          </ul>

          <!-- Authentication/user profile menu -->
          <ul class="navbar-nav">
            <li *ngIf="(isLoggedIn$ | async) === false" class="nav-item">
              <a class="nav-link" routerLink="/login" href="#">Sign in</a>
            </li>
            <li *ngIf="(isLoggedIn$ | async) === false" class="nav-item">
              <a class="nav-link" routerLink="/register" href="#">Sign up</a>
            </li>

            <li *ngIf="isLoggedIn$ | async as isLoggedIn" ngbDropdown class="nav-item dropdown">
              <a ngbDropdownToggle class="nav-link">Account</a>
              <div ngbDropdownMenu class="dropdown-menu" aria-labelledby="profile">
                <a class="dropdown-item" routerLink="" (click)="onLogout()">Logout</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  `,
  styles: [
    '.btn-link { color: rgba(255,255,255,.5); text-decoration: none; }',
    // tslint:disable-next-line: max-line-length
    '.btn-link.focus, .btn-link:focus, .btn-link.hover, .btn-link:hover { color: rgba(255,255,255,.75); text-decoration: none; box-shadow: none; }'
  ]
})
export class NavbarComponent implements OnInit {
  @Input() title: string
  isLoggedIn$: Observable<boolean>
  isNavbarCollapsed = true

  constructor(private authService: AuthService) {}
  /*eslint-disable */
  ngOnInit() {
    this.isLoggedIn$ = this.authService.userIsLoggedIn
  }
  /*eslint-enable */
  onLogout() {
    this.authService.logout()
  }
}

const express = require('express')
const router = express.Router()
const User = require('../models/user.model')
const logger = require('../config/applicationConfig').logger

router.post('/', async function(req, res) {
  try {
    logger.info('POST thread summoned')

    // checking the user is not a duplicate
    const foundUser = await User.find({ name: req.body.email }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (foundUser[0] === undefined) {
      // saving the user
      const user = new User(req.body)
      console.log(req.body)
      // tracing received object
      logger.trace(user)
      await user
        .save()
        .then(res.status(200).send({ name: user.email }))
        .catch(function(err) {
          res.status(400).end('Something went wrong')
          logger.error(err)
        })
    } else {
      res.status(406).end('Duplicates are not allowed')
    }
    // Error Handling
  } catch (err) {
    res.status(400).end()
    logger.error(err)
  }

  logger.info('POST succesful')
})

module.exports = router

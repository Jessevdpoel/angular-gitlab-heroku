import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { LoginComponent } from './login.component'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { Router } from '@angular/router'
import { AlertService } from 'src/app/modules/alert/alert.service'

describe('LoginComponent', () => {
  let component: LoginComponent
  let fixture: ComponentFixture<LoginComponent>
  let router: Router
  const loginServiceSpy = jasmine.createSpyObj('LoginService', ['login'])

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule, HttpClientTestingModule, RouterTestingModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent)
    component = fixture.componentInstance
    fixture.detectChanges()

    router = TestBed.get(Router)
    spyOn(router, 'navigateByUrl')
  })

  function updateForm(userEmail, userPassword) {
    component.loginForm.controls['email'].setValue(userEmail)
    component.loginForm.controls['password'].setValue(userPassword)
  }

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('component initial state', () => {
    expect(component.submitted).toBeFalsy()
    expect(component.loginForm).toBeDefined()
    expect(component.loginForm.invalid).toBeTruthy()
  })

  it('form value should update from when u change the input', () => {
    const validUser = {
      email: 'test@avans.nl',
      password: '123456'
    }
    updateForm(validUser.email, validUser.password)
    expect(component.loginForm.value).toEqual(validUser)
  })

  it('Form invalid should be true when form is invalid', () => {
    const blankUser = {
      email: '',
      password: ''
    }

    updateForm(blankUser.email, blankUser.password)
    expect(component.loginForm.invalid).toBeTruthy()
  })
})

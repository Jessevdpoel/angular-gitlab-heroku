import { Injectable } from '@angular/core'
import { Game } from './games/game/game'

import { HttpClient } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators'
import { MessageService } from './message.service'

@Injectable({
  providedIn: 'root'
})
export class GameService {
  apiUrlGame = '/api/games'
  constructor(private http: HttpClient, private messageService: MessageService) {}

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.apiUrlGame).pipe(
      tap(_ => this.log('fetched Games')),
      catchError(this.handleError<Game[]>('getGames', []))
    )
  }

  putGamesByName(game: Game): Observable<any> {
    const url = this.apiUrlGame + '/' + game.name
    return this.http.put<any>(url, game).pipe(
      tap(_ => this.log('updated Games')),
      catchError(this.handleError<Game>('putGames'))
    )
  }

  deleteGames(game: Game) {
    const url = this.apiUrlGame + '/' + game.name
    return this.http.delete<Game>(url).pipe(
      tap(_ => this.log('deleted Games')),
      catchError(this.handleError<Game>('deleteGames'))
    )
  }

  postGames(game: Game) {
    const url = this.apiUrlGame
    return this.http.post<Game>(url, game).pipe(
      tap(_ => this.log('posted Games')),
      catchError(this.handleError<Game>('postGames'))
    )
  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`)
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}

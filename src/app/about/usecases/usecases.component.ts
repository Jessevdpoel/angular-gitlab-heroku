import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Gebruiker',
      precondition: 'De actor moet geregistreerd zijn',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'Hiermee kan de gebruiker een account aanmaken',
      scenario: [
        'Gebruiker vult de vereiste velden in en klikt op submit',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien de gegevens correct zijn dan krijgt de gebruiker een validatie berichtje'
      ],
      actor: 'Gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is geregistreerd.'
    },
    {
      id: 'UC-03',
      name: 'Games toevoegen',
      description: 'Hiermee kan de gebruiker een game toevoegen aan de bestaande lijst met games',
      scenario: [
        'Gebruiker vult de vereiste velden in en klikt op submit',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien de gegevens correct zijn wordt de nieuwe game toegevoegd aan de bestaande lijst met games'
      ],
      actor: 'gebruiker',
      precondition: 'De actor is ingelogd',
      postcondition: 'Game wordt toegevoegd aan de bestaande lijst met games.'
    },
    {
      id: 'UC-04',
      name: 'Games verwijderen',
      description: 'Hiermee kan de gebruiker een game verwijderen uit de bestaande lijst met games',
      scenario: [
        'Gebruiker klikt op een game',
        'De applicatie haalt alle gegevens op en weergeeft deze',
        'Indien de gebruiker deze game wilt verwijderen klikt deze op delete.'
      ],
      actor: 'gebruiker',
      precondition: 'De actor is ingelogd en er moet een game in de lijst staan',
      postcondition: 'Game wordt verwijderd van de bestaande lijst met games.'
    },
    {
      id: 'UC-05',
      name: 'Games updaten',
      description: 'Hiermee kan de gebruiker een game updaten uit de bestaande lijst met games',
      scenario: [
        'Gebruiker klikt op een game',
        'De applicatie haalt alle gegevens op en weergeeft deze',
        'Indien de gebruiker deze game wilt updaten verandert hij de velden en klikt op update.'
      ],
      actor: 'gebruiker',
      precondition: 'De actor is ingelogd en er moet een game in de lijst staan',
      postcondition: 'Game wordt geupdate en veranderd in de bestaande lijst met games.'
    },
    {
      id: 'UC-06',
      name: 'Games bekijken',
      description: 'Hiermee kan de gebruiker een game bekijken uit de bestaande lijst met games',
      scenario: ['Gebruiker klikt op een game', 'De applicatie haalt alle gegevens op en weergeeft deze'],
      actor: 'gebruiker',
      precondition: 'De actor is ingelogd en er moet een game in de lijst staan',
      postcondition: 'Game wordt weergeven in de applicatie.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}

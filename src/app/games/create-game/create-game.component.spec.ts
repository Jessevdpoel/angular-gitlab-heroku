import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CreateGameComponent } from './create-game.component'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'

describe('CreateGameComponent', () => {
  let component: CreateGameComponent
  let fixture: ComponentFixture<CreateGameComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateGameComponent],
      imports: [FormsModule, HttpClientModule, NgbModule, RouterTestingModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGameComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

import { Component, OnInit, Input } from '@angular/core'
import { Developer } from '../developer/developer'
import { DeveloperService } from '../../developer.service'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-developer-detail',
  templateUrl: './developer-detail.component.html',
  styleUrls: ['./developer-detail.component.scss']
})
export class DeveloperDetailComponent implements OnInit {
  date
  constructor(
    private developerService: DeveloperService,
    private alertService: AlertService,
    private router: Router
  ) {}
  @Input() developer: Developer
  ngOnInit() {}

  save(): void {
    this.developer.founded = new Date(this.date.year, this.date.month, this.date.day)

    this.developerService.putDevelopersByName(this.developer).subscribe(data => {
      if (data === null) {
        this.alertService.error('Not allowed to update other users entities')
      } else {
        this.router.navigate(['/about'])
        this.alertService.success('Entity updated succesfully!')
      }
    })
  }

  delete(): void {
    this.developerService.deleteDevelopers(this.developer).subscribe(data => {
      if (data === null) {
        this.alertService.error('Not allowed to update other users entities')
      } else {
        this.router.navigate(['/about'])
        this.alertService.success('Entity updated succesfully!')
      }
    })
  }
}

import { Injectable } from '@angular/core'
import { Developer } from './developers/developer/developer'

import { HttpClient } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators'
import { MessageService } from './message.service'

@Injectable({
  providedIn: 'root'
})
export class DeveloperService {
  apiUrlDeveloper = '/api/developers'
  constructor(private http: HttpClient, private messageService: MessageService) {}

  getDevelopers(): Observable<Developer[]> {
    return this.http.get<Developer[]>(this.apiUrlDeveloper).pipe(
      tap(_ => this.log('fetched Developers')),
      catchError(this.handleError<Developer[]>('getDevelopers', []))
    )
  }

  putDevelopersByName(developer: Developer): Observable<any> {
    const url = this.apiUrlDeveloper + '/' + developer.name
    return this.http.put<any>(url, developer).pipe(
      tap(_ => this.log('updated Developers')),
      catchError(this.handleError<Developer>('putDevelopers'))
    )
  }

  deleteDevelopers(developer: Developer) {
    const url = this.apiUrlDeveloper + '/' + developer.name
    return this.http.delete<Developer>(url).pipe(
      tap(_ => this.log('deleted Developers')),
      catchError(this.handleError<Developer>('deleteDevelopers'))
    )
  }

  postDevelopers(developer: Developer) {
    const url = this.apiUrlDeveloper
    return this.http.post<Developer>(url, developer).pipe(
      tap(_ => this.log('posted Developers')),
      catchError(this.handleError<Developer>('postDevelopers'))
    )
  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`)
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}

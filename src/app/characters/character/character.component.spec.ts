import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CharacterComponent } from './character.component'
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'
import { Input, Component } from '@angular/core'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { RouterTestingModule } from '@angular/router/testing'

@Component({
  selector: 'app-character-detail',
  template: ''
})
class DeveloperDetailSubComponent {
  @Input() character
}

describe('CharacterComponent', () => {
  let component: CharacterComponent
  let fixture: ComponentFixture<CharacterComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharacterComponent, DeveloperDetailSubComponent],
      imports: [HttpClientModule, FormsModule, RouterTestingModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

import { Component, OnInit } from '@angular/core'
import { DeveloperService } from '../../developer.service'
import { Developer } from '../developer/developer'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-create-developer',
  templateUrl: './create-developer.component.html',
  styleUrls: ['./create-developer.component.scss']
})
export class CreateDeveloperComponent implements OnInit {
  date
  constructor(
    private developerService: DeveloperService,
    private alertService: AlertService,
    private router: Router
  ) {}
  developer: Developer

  ngOnInit() {
    this.developer = new Developer()
  }

  save(): void {
    this.developer.founded = new Date(this.date.year, this.date.month, this.date.day)
    this.developerService.postDevelopers(this.developer).subscribe(data => {
      if (data === undefined) {
        this.alertService.error('duplicates are not allowed')
      } else {
        console.log(data)
        this.router.navigate(['/developers'])
        this.alertService.success('character created succesfully')
      }
    })
  }
}

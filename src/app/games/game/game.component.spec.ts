import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { HttpClientModule } from '@angular/common/http'
import { GameComponent } from './game.component'
import { FormsModule } from '@angular/forms'
import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-game-detail',
  template: ''
})
class GameDetailSubComponent {
  @Input() game
}

describe('GameComponent', () => {
  let component: GameComponent
  let fixture: ComponentFixture<GameComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameComponent, GameDetailSubComponent],
      imports: [HttpClientModule, FormsModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

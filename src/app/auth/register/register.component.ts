import { Component, OnInit } from '@angular/core'
import { AuthService } from '../auth.service'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { User } from './user'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { Router } from '@angular/router'
import { LoginComponent } from '../login/login.component'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup
  subs: Subscription

  constructor(
    private authService: AuthService,
    private alertService: AlertService,
    private router: Router,
    private loginComponent: LoginComponent
  ) {}
  user: User

  ngOnInit() {
    this.registerForm = new FormGroup({
      name: new FormControl(),
      email: new FormControl(null, [Validators.required, this.loginComponent.validEmail.bind(this)]),
      password: new FormControl(null, [Validators.required, this.loginComponent.validPassword.bind(this)])
    })

    this.subs = this.authService.userIsLoggedIn.subscribe(alreadyLoggedIn => {
      if (alreadyLoggedIn) {
        console.log('User already logged in > to about')
        this.router.navigate(['/about']).catch(e => {
          console.log(e)
        })
      }
    })
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.user = new User()
      this.user.email = this.registerForm.value.email
      this.user.password = this.registerForm.value.password
      this.user.name = this.registerForm.value.name
      this.authService.register(this.user).subscribe(
        data => {
          this.alertService.success('Registratie succesvol')
        },
        error => {
          this.alertService.error('Email is al in gebruik')
        }
      )
    } else {
      this.alertService.error('registerForm invalid')
      console.error('registerForm invalid')
    }
  }
}

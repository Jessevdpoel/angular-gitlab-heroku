import { Component, OnInit } from '@angular/core'
import { Developer } from './developer'
import { DeveloperService } from '../../developer.service'

@Component({
  selector: 'app-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.scss']
})
export class DeveloperComponent implements OnInit {
  developers$: Developer[]
  selectedDeveloper: Developer

  constructor(private developerService: DeveloperService) {
    this.developers$ = []
  }

  ngOnInit() {
    this.developerService.getDevelopers().subscribe(data => (this.developers$ = data))
  }

  onSelect(developer: Developer): void {
    this.selectedDeveloper = developer
  }
}

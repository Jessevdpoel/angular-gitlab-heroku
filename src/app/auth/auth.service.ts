import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { User } from '../auth/register/user'
import { Router } from '@angular/router'
import { environment } from '../../environments/environment'
import { map, tap, catchError } from 'rxjs/operators'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isLoggedInUser = new BehaviorSubject<boolean>(false)
  public loggedInEmail = new BehaviorSubject<string>('')

  private readonly currentUser = 'currentuser'
  private currentToken = 'token'

  // store the URL so we can redirect after logging in
  public readonly redirectUrl: string = '/about'
  public readonly loginredirect: string = '/login'
  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private alertService: AlertService, private router: Router, private http: HttpClient) {
    this.getCurrentUser().subscribe({
      next: (user: User) => {
        console.log(`${user.email} logged in`)
        this.isLoggedInUser.next(true)
        this.loggedInEmail.next(user.email)
        // Verify the possible roles the user can have.
        // Add more verifications when nessecary.
      },
      error: message => {
        this.router.navigate([this.loginredirect])
      }
    })
  }
  login(email: string, password: string) {
    console.log('login')
    console.log(`POST ${environment.apiUrl}/api/login`)

    return this.http
      .post(`${environment.apiUrl}/api/login`, { email, password }, { headers: this.headers })
      .pipe(
        tap(
          data => console.log(data),
          error => console.error(error)
        )
      )
      .subscribe({
        next: (response: any) => {
          const currentUser = new User(response)
          console.dir(currentUser)
          this.saveCurrentUser(currentUser, response.token)
          // Notify all listeners that we're logged in.
          this.isLoggedInUser.next(true)
          this.loggedInEmail.next(currentUser.email)
          this.alertService.success('You have been logged in')
          // If redirectUrl exists, go there
          this.router.navigate([this.redirectUrl])
        },
        error: (message: any) => {
          console.log('error:', message)
          this.alertService.error('Invalid credentials')
        }
      })
  }

  /**
   * Log out.
   */
  logout() {
    console.log('logout')
    localStorage.removeItem(this.currentUser)
    localStorage.removeItem(this.currentToken)
    this.isLoggedInUser.next(false)
    this.router.navigate(['/login'])
    this.alertService.success('You have been logged out.')
  }

  /**
   * Get the currently logged in user.
   */
  public getCurrentUser(): Observable<User> {
    return new Observable(observer => {
      const localUser: any = JSON.parse(localStorage.getItem(this.currentUser))
      console.log('localUser', localUser)
      if (localUser) {
        console.log('localUser found')
        observer.next(new User(localUser))
        observer.complete()
      } else {
        console.log('NO localUser found')
        observer.error('NO localUser found')
        observer.complete()
      }
    })
  }

  /**
   *
   */
  private saveCurrentUser(user: User, token: string): void {
    localStorage.setItem(this.currentUser, JSON.stringify(user))
    localStorage.setItem(this.currentToken, token)
  }
  /**
   *
   */
  get userIsLoggedIn(): Observable<boolean> {
    console.log('userIsLoggedIn() ' + this.isLoggedInUser.value)
    return this.isLoggedInUser.asObservable()
  }

  /**
   *
   */
  register(user: User): Observable<User> {
    return this.http.post(`${environment.apiUrl}/api/register`, user).pipe(
      map(result => new User(result)),

      tap(
        data => console.log(data),
        error => console.log(error)
      )
    )
  }

  get token(): string {
    console.log('token() ' + this.currentToken)
    const localToken: any = JSON.parse(localStorage.getItem(this.currentToken))
    return localToken
  }
  public getCurrentToken() {
    return this.currentToken
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CreateCharacterComponent } from './create-character.component'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'

describe('CreateCharacterComponent', () => {
  let component: CreateCharacterComponent
  let fixture: ComponentFixture<CreateCharacterComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateCharacterComponent],
      imports: [FormsModule, HttpClientModule, RouterTestingModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCharacterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

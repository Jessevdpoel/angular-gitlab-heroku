import { TestBed } from '@angular/core/testing'
import { HttpClientModule } from '@angular/common/http'
import { DeveloperService } from './developer.service'

describe('DeveloperService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    })
  )

  it('should be created', () => {
    const service: DeveloperService = TestBed.get(DeveloperService)
    expect(service).toBeTruthy()
  })
})

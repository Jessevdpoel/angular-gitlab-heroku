const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  name: {
    type: String,
    required: [true, 'creating a User requires a name']
  },
  email: {
    type: String,
    required: [true, 'creating a User requires a email'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'A user requires a password']
  }
})

// Creating user model
const User = mongoose.model('user', UserSchema)

// Exporting the User
module.exports = User

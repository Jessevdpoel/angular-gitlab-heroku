import { Component, OnInit } from '@angular/core'
import { CharacterService } from '../../character.service'
import { Character } from './character'

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {
  characters$: Character[]
  selectedCharacter: Character

  constructor(private characterService: CharacterService) {
    this.characters$ = []
  }

  ngOnInit() {
    this.characterService.getCharacters().subscribe(data => (this.characters$ = data))
  }

  onSelect(character: Character): void {
    this.selectedCharacter = character
  }
}

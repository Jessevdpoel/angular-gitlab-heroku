const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CharacterSchema = new Schema({
  name: {
    type: String,
    required: [true, 'creating a Character requires a name'],
    unique: true
  },
  series: {
    type: String,
    required: [true, 'Creating a Character requires a series']
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user'
  }
})

// Creating developer model
const Character = mongoose.model('character', CharacterSchema)

// Exporting the Character
module.exports = Character

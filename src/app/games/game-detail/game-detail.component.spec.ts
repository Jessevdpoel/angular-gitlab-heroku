import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GameDetailComponent } from './game-detail.component'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { Component } from '@angular/core'
import { Developer } from '../../developers/developer/developer'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'

@Component({
  selector: 'app-asdasd',
  template: '<app-game-detail [game]="input"></app-game-detail>'
})
class TestHostComponent {
  input = {
    name: 'nintendo',
    desciption: 'mario',
    releaseDate: new Date(),
    developer: new Developer(),
    rating: 5
  }
}

describe('GameDetailComponent', () => {
  let component: TestHostComponent
  let fixture: ComponentFixture<TestHostComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameDetailComponent, TestHostComponent],
      imports: [FormsModule, HttpClientModule, NgbModule, RouterTestingModule],
      providers: [AlertService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})

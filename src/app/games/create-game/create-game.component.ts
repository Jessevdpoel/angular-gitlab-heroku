import { Component, OnInit } from '@angular/core'
import { GameService } from '../../game.service'
import { Game } from '../game/game'
import { DeveloperService } from '../../developer.service'
import { Developer } from '../../developers/developer/developer'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.scss']
})
export class CreateGameComponent implements OnInit {
  date
  constructor(
    private gameService: GameService,
    private developerService: DeveloperService,
    private alertService: AlertService,
    private router: Router
  ) {}
  game: Game
  developers$: Developer[]

  ngOnInit() {
    this.game = new Game()
    this.developerService.getDevelopers().subscribe(data => (this.developers$ = data))
  }

  save(): void {
    this.game.releaseDate = new Date(this.date.year, this.date.month, this.date.day)
    this.gameService.postGames(this.game).subscribe(data => {
      if (data === undefined) {
        this.alertService.error('duplicates are not allowed')
      } else {
        console.log(data)
        this.router.navigate(['/games'])
        this.alertService.success('Game created succesfully')
      }
    })
  }
}

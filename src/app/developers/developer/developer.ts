export class Developer {
  name: string
  founded: Date
  founder: string
  employees: number
}

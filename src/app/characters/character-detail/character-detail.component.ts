import { Component, OnInit, Input } from '@angular/core'
import { Character } from '../character/character'
import { CharacterService } from '../../character.service'
import { AlertService } from 'src/app/modules/alert/alert.service'

import { Router } from '@angular/router'

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.scss']
})
export class CharacterDetailComponent implements OnInit {
  constructor(
    private characterService: CharacterService,
    private alertService: AlertService,
    private router: Router
  ) {}
  @Input() character: Character

  ngOnInit() {}

  save(): void {
    this.characterService.putCharactersByName(this.character).subscribe(data => {
      if (data === null) {
        this.alertService.error('Not allowed to update other users entities')
      } else {
        this.router.navigate(['/about'])
        this.alertService.success('Entity updated succesfully!')
      }
    })
  }

  delete(): void {
    this.characterService.deleteCharacters(this.character).subscribe(data => {
      if (data === null) {
        this.alertService.error('Not allowed to delete other users entities')
      } else {
        this.router.navigate(['/about'])
        this.alertService.success('Entity deleted succesfully!')
      }
    })
  }
}

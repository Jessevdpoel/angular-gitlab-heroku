import { Component, OnInit } from '@angular/core'
import { CharacterService } from '../../character.service'
import { Character } from '../character/character'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-create-character',
  templateUrl: './create-character.component.html',
  styleUrls: ['./create-character.component.scss']
})
export class CreateCharacterComponent implements OnInit {
  date
  constructor(
    private characterService: CharacterService,
    private alertService: AlertService,
    private router: Router
  ) {}
  character: Character

  ngOnInit() {
    this.character = new Character()
  }

  save(): void {
    this.characterService.postCharacters(this.character).subscribe(data => {
      if (data === undefined) {
        this.alertService.error('duplicates are not allowed')
      } else {
        console.log(data)
        this.router.navigate(['/characters'])
        this.alertService.success('character created succesfully')
      }
    })
  }
}

const express = require('express')
const router = express.Router()
const Developer = require('../models/developer.model')
const logger = require('../config/applicationConfig').logger
const jwt = require('jsonwebtoken')
router.post('/', async function(req, res) {
  try {
    logger.info('POST thread summoned')

    const authorization = req.headers.authorization.split(' ')[1]
    let userId = jwt.verify(authorization, req.app.get('secretkey'))
    req.body.user = userId.id

    // checking the developer is not a duplicate
    const foundDeveloper = await Developer.find({ name: req.body.name }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (foundDeveloper[0] === undefined) {
      // saving the developer
      const developer = new Developer(req.body)
      console.log(req.body)
      // tracing received object
      logger.trace(developer)
      await developer
        .save()
        .then(res.status(200).send({ name: developer.name }))
        .catch(function(err) {
          res.status(400).end('Something went wrong')
          logger.error(err)
        })
    } else {
      res.status(406).end('Duplicates are not allowed')
    }
    // Error Handling
  } catch (err) {
    res.status(400).end()
    logger.error(err)
  }

  logger.info('POST succesful')
})

// Get All
router.get('/', async function(req, res) {
  logger.info('GET all developers summoned')

  // Defining variables

  // Finding all developers
  const developers = await Developer.find()
    .populate('developers')
    .catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

  // Response status
  res.status(200).send(developers)
})

router.get('/:name', async function(req, res) {
  logger.info('GET Developer by name summoned')
  // Defining variables
  const developerName = req.params.name
  // Requesting server for specific thread
  try {
    // Finding thread by ID
    let foundDeveloper = await Developer.find({ name: developerName }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    // If there is no thread a bad request is send as respons
    if (developerName === foundDeveloper[0].name) {
      try {
        // Response status
        res.status(200).send(foundDeveloper)
      } catch (err) {
        // Response status
        res.status(404).end(err)
      }
    } else {
      logger.info('er ging iets fout bel alstublieft: 0642979245')
    }
  } catch (err) {
    // Response status
    res.status(204).end('Id bestaat niet')
  }
})

// Delete thread on id
router.delete('/:name', async function(req, res) {
  logger.info('DELETE Developer by name summoned')
  // Defining variables

  const authorization = req.headers.authorization.split(' ')[1]
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  req.body.user = userId.id

  const developerName = req.params.name
  // Trying to find the developer by Name
  try {
    // Searching by Name
    let foundDeveloper = await Developer.find({ name: developerName }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (req.body.user !== foundDeveloper[0].user.toString()) {
      res.status(403).end({
        message: 'Not allowed to modify other users entity!',
        code: 403
      })
    }
    // If there is no developer a bad request is send
    if (developerName === foundDeveloper[0].name) {
      try {
        await Developer.deleteOne({ name: developerName }).catch(function(error) {
          res.status(400).send(error)
          logger.error(error)
        })

        // Response status
        res.status(200).send(foundDeveloper)
        // Error handling
      } catch (err) {
        res.status(400).end(err)
      }
      // Error handling
    } else {
      res.status(400).end('No Developer found please make sure you entered the right name')
    }
    // Error handling
  } catch (err) {
    res.status(204).end('Naam bestaat niet kijk nog een keer goed naar wat u ingevuld hebt')
  }
})

router.put('/:name', async function(req, res) {
  logger.info('PUT by Name summoned')

  const founded = req.body.founded
  const founder = req.body.founder
  const authorization = req.headers.authorization.split(' ')[1]
  const employees = req.body.employees
  let userId = jwt.verify(authorization, req.app.get('secretkey'))
  const developerName = req.params.name

  // Defining variables
  req.body.user = userId.id
  // Trying to find the developer by Name
  try {
    // Searching by Name
    let foundDeveloper = await Developer.find({ name: developerName }).catch(function(error) {
      res.status(400).send(error)
      logger.error(error)
    })

    if (foundDeveloper[0].user.toString() !== req.body.user) {
      res.status(403).end({
        message: 'Not allowed to modify other users entity! Please confirm it is your own enitity'
      })
    }

    // If there is no developer a bad request is send
    if (developerName === foundDeveloper[0].name) {
      try {
        await Developer.updateOne(
          { name: developerName },
          { $set: { founded: founded, founder: founder, employees: employees } }
        ).catch(function(error) {
          res.status(400).send(error)
          logger.error(error)
        })

        // Response status
        res.status(200).send(foundDeveloper)
        // Error handling
      } catch (err) {
        res.status(400).end(err)
      }
      // Error handling
    } else {
      logger.info('Something went wrong, please call: 0630866874 or 0642979245')
      res.status(400).end('No Character found')
    }
    // Error handling
  } catch (err) {
    res.status(204).end('Naam bestaat niet')
  }
})

module.exports = router
